const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function (env, argv) {
  const _config = {
    entry: "./src/js/main.js",
    output: {
      filename: "./js/bundle.js",
      //clean: true, // Limpia la carpeta dist antes de ejecutar webpack (deberá ejecutar la consola como administrador).
    },
    module: {
      rules: [
        // 'npm run dev / watch' => Carga los estilos (manteniendo los ficheros .scss para depurar desde el inspector)
        {
          test: /\.scss$/,
          use: [
            {
              loader: "style-loader",
              options: {},
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
              },
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true,
              },
            },
          ],
        },
      ],
    },
  };

  // 'npm run build' => Minifica el Sass en un CSS.
  if (argv.mode === "production") {
    _config.plugins = [
      new MiniCssExtractPlugin({
        filename: "./css/styles.css",
      }),
    ];
    _config.module.rules.push({
      test: /\.scss$/,
      use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
    });
  }

  return _config;
};
