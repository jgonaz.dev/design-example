# design-example

Calcar lo maximo posible diseño e interacción enlace Figma.

## Project setup
```
npm install
```

## Compila el bundle y carga los estilos (manteniendo los SCSS para depurar desde el inspector).
```
npm run dev
```

## Compila el bundle y recarga los cambios detectados en tiempo real.
```
npm run watch
```

## Compila y minifica el bundle + un CSS con los estilos.
```
npm run build
```
